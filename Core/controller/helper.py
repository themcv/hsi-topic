"""
Created by Murat Can VARER 19.12.20
"""

import random
import os

import numpy as np
import scipy.io as sio
import os
from datetime import date
import pandas as pd
import tensorflow as tf
from sklearn.metrics import classification_report, accuracy_score, confusion_matrix, cohen_kappa_score
from operator import truediv

def do_uuid():
    return random.randint(1000000, 9999999)


def fileCheck():
    if not os.path.exists(os.path.dirname("results")):
        try:
            os.makedirs("results")
            os.makedirs("results/table")

            os.makedirs("results/visualization")
            os.makedirs("results/visualization/IP")
            os.makedirs("results/visualization/KSC")
            os.makedirs("results/visualization/PC")
            os.makedirs("results/visualization/SA")
            os.makedirs("results/visualization/UP")
            os.makedirs("results/visualization/BW")

            os.makedirs("results/inferences")
            os.makedirs("results/inferences/IP")
            os.makedirs("results/inferences/KSC")
            os.makedirs("results/inferences/PC")
            os.makedirs("results/inferences/SA")
            os.makedirs("results/inferences/UP")
            os.makedirs("results/inferences/BW")
        except OSError as err:
            print("File has been created. No problem !")


def Patch(data, height_index, width_index, PATCH_SIZE):
    height_slice = slice(height_index, height_index + PATCH_SIZE)
    width_slice = slice(width_index, width_index + PATCH_SIZE)
    patch = data[height_slice, width_slice, :]

    return patch


def loadData(config):
    data_path = os.path.join(os.getcwd() + "/Dataset/" + config.dataset.name + "/")
    # print(data_path)
    if config.dataset.name == 'IP':
        data = sio.loadmat(os.path.join(data_path, 'Indian_pines.mat'))['indian_pines']
        labels = sio.loadmat(os.path.join(data_path, 'Indian_pines_gt.mat'))['indian_pines_gt']
    elif config.dataset.name == 'SA':
        data = sio.loadmat(os.path.join(data_path, 'Salinas.mat'))['salinas']
        labels = sio.loadmat(os.path.join(data_path, 'Salinas_gt.mat'))['salinas_gt']
    elif config.dataset.name == 'UP':
        data = sio.loadmat(os.path.join(data_path, 'PaviaU.mat'))['paviaU']
        labels = sio.loadmat(os.path.join(data_path, 'PaviaU_gt.mat'))['paviaU_gt']
    elif config.dataset.name == 'KSC':
        data = sio.loadmat(os.path.join(data_path, 'KSC.mat'))['KSC']
        labels = sio.loadmat(os.path.join(data_path, 'KSC_gt.mat'))['KSC_gt']
    elif config.dataset.name == 'PC':
        data = sio.loadmat(os.path.join(data_path, 'Pavia.mat'))['pavia']
        labels = sio.loadmat(os.path.join(data_path, 'Pavia_gt.mat'))['pavia_gt']
    elif config.dataset.name == 'BW':
        data = sio.loadmat(os.path.join(data_path, 'Botswana.mat'))['Botswana']
        labels = sio.loadmat(os.path.join(data_path, 'Botswana_gt.mat'))['Botswana_gt']
    return data, labels


def saveResultCSV(config, datasetName="IP", modelId=151216521,
                  kappa=0.98, overall=99.2, average=99.9):
    overall = "{0:.3f}".format(overall)
    kappa = "{0:.3f}".format(kappa)
    average = "{0:.3f}".format(average)

    dict = {'Dataset Name': [datasetName], 'Model Id': [modelId],
            'Kappa(%)': [kappa], 'Overall Accuracy': [overall], 'Average Accuracy': [average],
            'Date': date.today()
            }

    df = pd.DataFrame(dict)
    if not os.path.isfile(config.saveCsvResults):
        df.to_csv(config.saveCsvResults, index=False)
    else:
        df.to_csv(config.saveCsvResults, mode='a', header=False, index=False)


def gpuMemLimiter():
    gpus = tf.config.experimental.list_physical_devices('GPU')
    if gpus:
        try:
            for gpu in gpus:
                tf.config.experimental.set_memory_growth(gpu, True)
        except RuntimeError as e:
            print(e)

def AA_andEachClassAccuracy(confusion_matrix):
    counter = confusion_matrix.shape[0]
    list_diag = np.diag(confusion_matrix)
    list_raw_sum = np.sum(confusion_matrix, axis=1)
    each_acc = np.nan_to_num(truediv(list_diag, list_raw_sum))
    average_acc = np.mean(each_acc)
    return each_acc, average_acc


def reports(Xtest, y_test, y_pred, name, model):
    if name == 'IP':
        target_names = ['Alfalfa', 'Corn-notill', 'Corn-mintill', 'Corn',
                        'Grass-pasture', 'Grass-trees', 'Grass-pasture-mowed',
                        'Hay-windrowed', 'Oats', 'Soybean-notill', 'Soybean-mintill',
                        'Soybean-clean', 'Wheat', 'Woods', 'Buildings-Grass-Trees-Drives',
                        'Stone-Steel-Towers']
    elif name == 'SA':
        target_names = ['Brocoli_green_weeds_1', 'Brocoli_green_weeds_2', 'Fallow', 'Fallow_rough_plow',
                        'Fallow_smooth',
                        'Stubble', 'Celery', 'Grapes_untrained', 'Soil_vinyard_develop', 'Corn_senesced_green_weeds',
                        'Lettuce_romaine_4wk', 'Lettuce_romaine_5wk', 'Lettuce_romaine_6wk', 'Lettuce_romaine_7wk',
                        'Vinyard_untrained', 'Vinyard_vertical_trellis']
    elif name == 'PU':
        target_names = ['Asphalt', 'Meadows', 'Gravel', 'Trees', 'Painted metal sheets', 'Bare Soil', 'Bitumen',
                        'Self-Blocking Bricks', 'Shadows']

    classification = classification_report(np.argmax(y_test, axis=1), y_pred, target_names=target_names)
    oa = accuracy_score(np.argmax(y_test, axis=1), y_pred)
    confusion = confusion_matrix(np.argmax(y_test, axis=1), y_pred)
    each_acc, aa = AA_andEachClassAccuracy(confusion)
    kappa = cohen_kappa_score(np.argmax(y_test, axis=1), y_pred)
    score = model.evaluate(Xtest, y_test, batch_size=32)
    Test_Loss = score[0] * 100
    Test_accuracy = score[1] * 100

    return str(classification), str(confusion), Test_Loss, Test_accuracy, \
           oa * 100, each_acc * 100, aa * 100, kappa * 100